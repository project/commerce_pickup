<?php

namespace Drupal\commerce_pickup;

use Drupal\commerce_shipping\ProfileFieldCopy;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_product\Entity\ProductVariationType;

/**
 * Removes the billing is the same as shipping if the plugin method is pickuped.
 */
class PickupProfileFieldCopy extends ProfileFieldCopy {

  /**
   * {@inheritdoc}
   */
  public function supportsForm(array &$inline_form, FormStateInterface $form_state) {
    if ($form_state->get('pickuped')) {
      $form_state->set('pickuped', FALSE);

      return $form_state->get('pickuped');
    }

    if ($inline_form['#profile_scope'] !== 'shipping') {
      $order = parent::getOrder($form_state);
      $order_items = $order ? $order->getItems() : [];
      foreach ($order_items as $order_item) {
        $variation_type = ProductVariationType::load($order_item->getPurchasedEntity()->bundle());
        // At least one variation type must be shippable.
        if ($variation_type->hasTrait("purchasable_entity_shippable")) {
          return parent::supportsForm($inline_form, $form_state);
        }
      }
      return FALSE;
    }

    return parent::supportsForm($inline_form, $form_state);
  }

}

